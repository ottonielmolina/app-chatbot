import {
  optionCards,
  selectField,
  tagsField,
  textField,
  disabledFieldText,
  endOfConversation,
} from '../StateFormatter';
import * as RTypes from '../responseTypes';

const common_greetings = /(^hello|^hllo|^hi|^hey|^hola|^sup)\b\s?.*$/i;
const common_greetings_negative = /(?!(^hello|^hi|^hey|^hllo|^sup|^hola)\b)\w+/i;

const questions = {
  start: {
    botPrompt: 'Hola mi nombre es <strong>Chatbot</strong> y seré tu aliado para ayudarte a decidir sobre tu futuro.',
    answers: [
      {
        nextId: 'proposito',
      },
    ],
  },
  proposito: {
    botPrompt: 'Mi propósito es guiarte para tomar una decisión acerca del ámbito académico',
    answers: [
      {
        nextId: 'yourName',
      },
    ],
  },
  yourName: {
    botPrompt: 'Bien, ya te he hablado sobre mí, cuál es tu nombre?',
    input: textField(),
    answers: [
      {
        answer: common_greetings,
        nextId: 'greetings_notAName',
      },
      {
        answer: common_greetings_negative,
        catchName: true,
        nextId: 'asYouCanSee',
      },
    ],
  },
  greetings_notAName: {
	  botPrompt: 'Hey! <strong>Aún me encuentro entrenando para hablar con humanos</strong>, lo cual significa que mi rango de conversación no es muy variado 😅, lo siento',
	  answers: [
	    {
	      nextId: 'greetings_whatsYourNameAgain',
	    },
	  ],
  },
  greetings_whatsYourNameAgain: {
    botPrompt: 'Cuál es tu nombre?',
    varName: '@varName',
    input: textField(),
	  answers: [
	    {
	      answer: common_greetings,
	      nextId: 'greetings_notAName',
	    },
	    {
	      answer: common_greetings_negative,
	      catchName: true,
	      nextId: 'asYouCanSee',
	    },
	  ],
  },
  asYouCanSee: {
    botPrompt: 'De acuerdo <strong>@varName</strong>, podrías indicar la región en ciudad  de Guatemala donde resides.',
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField(['Sur', 'Norte', 'Centro', 'Oriente', 'Poniente']),
    answers: [
      {
        answer: 'Sur', // USAC, UFM
        nextId: 'univ_Sur'
      },
      {
        answer: 'Norte', // UMG
        nextId: 'univ_Norte'
      },
      {
        answer: 'Centro', // UMG, UFM
        nextId: 'univ_Centro'
      },
      {
        answer: 'Oriente', // UFM, UVG
        nextId: 'univ_Oriente'
      },
      {
        answer: 'Poniente', // UMG, USAC
        nextId: 'univ_Poniente'
      }
    ]
  },
  univ_Sur: {
    botPrompt: "Por la ubicación y la cercanía podré recomendarle la <span style='color:purple; background-color:white;font-weight:bold'>Universidad San Carlos de Guatemala</span> y la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Francisco Marroquín</span>.",
    answers: [
			{ nextId: 'facultad1' },
    ],
  },
  univ_Norte: {
    botPrompt: "Por la ubicación y la cercanía podré recomendarle la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Mariano Gálvez de Guatemala</span>.",
    answers: [
			{ nextId: 'facultad1' },
    ],
  },
  univ_Centro: {
    botPrompt: "Por la ubicación y la cercanía podré recomendarle la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Mariano Gálvez de Guatemala</span> y la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Francisco Marroquín</span>.",
    answers: [
			{ nextId: 'facultad1' },
    ],
  },
  univ_Oriente: {
    botPrompt: "Por la ubicación y la cercanía podré recomendarle la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Francisco Marroquín</span> y la <span style='color:purple; background-color:white;font-weight:bold'>Universidad del Valle de Guatemala</span>.",
    answers: [
			{ nextId: 'facultad1' },
    ],
  },
  univ_Poniente: {
    botPrompt: "Por la ubicación y la cercanía podré recomendarle la <span style='color:purple; background-color:white;font-weight:bold'>Universidad Mariano Gálvez de Guatemala</span> y la <span style='color:purple; background-color:white;font-weight:bold'>Universidad San Carlos de Guatemala</span>.",
    answers: [
			{ nextId: 'facultad1' },
    ],
  },
  facultad1: {
    botPrompt: "Ahora <strong>@varName</strong> veamos cuales son tus intereses, puedes seleccionar alguno de los que te muestro abajo?",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Encontrar cura a enfermedades', 'Tecnología', 'Finanzas de una empresa', 'Administrar tu propia empresa'
    ]),
    answers: [{ nextId: 'facultad2' }]
  },
  facultad2: {
    botPrompt: "Puedes seleccionar alguno de estos otros que te muestro por favor?",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Curar y atender pacientes', 'Pasión por el ingenio', 'La contabilidad de empresas', 'Formar tu propia empresa'
    ]),
    answers: [
      { answer: 'Curar y atender pacientes', nextId: 'facMedicina' },
      { answer: 'Pasión por el ingenio', nextId: 'facIngenieria' },
      { answer: 'La contabilidad de empresas', nextId: 'facEconomicas' },
      { answer: 'Formar tu propia empresa', nextId: 'facAdmon' },
    ]
  },

  facMedicina: {
    botPrompt: "Ok, podría recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>facultad de medicina</span>, ya que eres altruísta y quieres ver el bien del mundo en cuanto a salud se refiere, aunque déjame comentarte que hay especializaciones.",
    answers: [{ nextId: 'carreraFacMedicina' }],
  },
  facIngenieria: {
    botPrompt: "Ok, podría recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>facultad de ingeniería</span>, ya que te gusta la tecnología y eres ingenioso, aunque déjame comentarte que hay especializaciones.",
    answers: [{ nextId: 'carreraFacIngenieria' }],
  },
  facEconomicas: {
    botPrompt: "Ok, podría recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>facultad de ciencias económicas</span>, ya que te gusta la contabilidad y las finanzas, aunque déjame comentarte que hay especializaciones.",
    answers: [{ nextId: 'carreraFacEconomicas' }],
  },
  facAdmon: {
    botPrompt: "Ok, podría recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>facultad de administración de empresas</span>, ya que lo tuyo es la organizacion de empresas y el orden, aunque déjame comentarte que hay especializaciones.",
    answers: [{ nextId: 'carreraFacAdmon' }],
  },

// Carreras de Medicina
  carreraFacMedicina: {
    botPrompt: "En la facultad de medicina existen varias especializaciones, solo es de ver que es lo que más te atrae <strong>@varName</strong>.",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Estar en forma', 'Gusto por los animales', 'Masajes y terapia', 'Atención de pacientes'
    ]),
    answers: [
      { answer: 'Estar en forma', nextId: 'carrNutricion' },
      { answer: 'Gusto por los animales', nextId: 'carrVeterinaria' },
      { answer: 'Masajes y terapia', nextId: 'carrFisioterapia' },
      { answer: 'Atención de pacientes', nextId: 'carrEnfermeria' },
    ]
  },
  carrNutricion: {
    botPrompt: "Si te interesa estar en forma, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de nuetrición</span>, aprenderás el arte del buen comer para mantenerte sano y en forma.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrVeterinaria: {
    botPrompt: "Si tienes gusto por los animales, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de veterinaria</span>, aprenderás las prácticas para mantener a tu mascota saludable.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrFisioterapia: {
    botPrompt: "Si te interesan las técnicas de relajación, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de fisioterapia</span>, aprenderás las técnicas de dar masajes y estar libre de estrés.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrEnfermeria: {
    botPrompt: "Si te interesa la atención a pacientes, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de enfermería</span>, aprenderás como es el trato a los pacientes de un nosocomio.",
    answers: [{ nextId: 'jornada1' }],
  },

// Carreras de Ingenieria
  carreraFacIngenieria: {
    botPrompt: "En la facultad de ingeniería existen varias especializaciones, solo es de ver que es lo que más te atrae <strong>@varName</strong>.",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Construcción y obras', 'Pasión por el software', 'Circuitos electrónicos', 'Vehículos motorizados'
    ]),
    answers: [
      { answer: 'Construcción y obras', nextId: 'carrCivil' },
      { answer: 'Pasión por el software', nextId: 'carrSistemas' },
      { answer: 'Circuitos electrónicos', nextId: 'carrElectronica' },
      { answer: 'Vehículos motorizados', nextId: 'carrMecanica' },
    ]
  },
  carrCivil: {
    botPrompt: "Si te interesa la construcción de obras, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de ingeniería civil</span>, aprenderás todo lo relacionado al diseño de obras arquitectónicas.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrSistemas: {
    botPrompt: "Si tienes pasión por el software, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de ingeniería en sistemas</span>, aprenderás todas las fases del ciclo de desarrollo del software.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrElectronica: {
    botPrompt: "Si te llama la atención los circuitos electrónicos, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de ingeniería electrónica</span>, aprenderás teoría y realizarás práctica de construcción de circuitos electrónicos.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrMecanica: {
    botPrompt: "Si te interesan los motores, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de ingeniería mecánica</span>, aprenderás como aplicar las ciencias exactas que utilizan los vehiculos motorizados para funcionar.",
    answers: [{ nextId: 'jornada1' }],
  },

// Carreras de Economicas
  carreraFacEconomicas: {
    botPrompt: "En la facultad de ciencias económicas existen varias especializaciones, solo es de ver que es lo que más te atrae <strong>@varName</strong>.",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Auditoría de finanzas', 'Relaciones con el extranjero', 'La economía de una empresa'
    ]),
    answers: [
      { answer: 'Auditoría de finanzas', nextId: 'carrAuditoria' },
      { answer: 'Relaciones con el extranjero', nextId: 'carrComercio' },
      { answer: 'La economía de una empresa', nextId: 'carrEconomia' }
    ]
  },
  carrAuditoria: {
    botPrompt: "Si te interesa la auditoría de finanzas, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de contaduría pública y auditoría</span>, aprenderás todo lo relacionado a las finanzas y como llevarlas al día.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrComercio: {
    botPrompt: "Si tienes pasión por las relaciones con el extranjero, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de comercio internacional</span>, aprenderás todas las técnicas de comercio con el extranjero y las relaciones internacionales.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrEconomia: {
    botPrompt: "Si te gusta la economía, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de economista</span>, aprenderás todas las tareas que ejecuta un economista para gestionar las finanzas de una empresa.",
    answers: [{ nextId: 'jornada1' }],
  },

  // Carreras de Administración
  carreraFacAdmon: {
    botPrompt: "En la facultad de administración existen varias especializaciones, solo es de ver que es lo que más te atrae <strong>@varName</strong>.",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: selectField([
        'Gestión de la empresa', 'Publicidad y mercadeo', 'Dirección estratégica'
    ]),
    answers: [
      { answer: 'Gestión de la empresa', nextId: 'carrAdministrador' },
      { answer: 'Publicidad y mercadeo', nextId: 'carrMercadeo' },
      { answer: 'Dirección estratégica', nextId: 'carrGerencia' }
    ]
  },
  carrAdministrador: {
    botPrompt: "Si te interesa la gestión de empresas, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de administrador de empresas</span>, aprenderás todas las técnicas para gestionar una empresa.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrMercadeo: {
    botPrompt: "Si te gusta la publicidad y el mercadeo, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de mercadotecnia</span>, aprenderás todas las técnicas de publicidad para dar a conocer tu marca.",
    answers: [{ nextId: 'jornada1' }],
  },
  carrGerencia: {
    botPrompt: "Si te llama la atención la dirección estratégica, puedo recomendarte la <span style='color:purple; background-color:white;font-weight:bold'>carrera de gerencia</span>, aprenderás las técnicas necesarias para definir las estrategias de una organización.",
    answers: [{ nextId: 'jornada1' }],
  },


  jornada1: {
    botPrompt: "Bien <strong>@varName</strong>, ahora veamos respecto a la jornada de la carrera.",
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    answers: [{ nextId: 'jornada2' }]
  },
  jornada2: {
    botPrompt: "Actualmente posees trabajo?",
    input: selectField(['Sí', 'No']),
    answers: [
      { answer: 'Sí', nextId: 'jornada4' },
      { answer: 'No', nextId: 'jornada3' }
    ]
  },

  jornada3: {
    botPrompt: "Auch, que lo siento, pero no te preocupes que pronto encotrarás el trabajo que has estado buscando",
    answers: [{ nextId: 'jornada55' }]
  },
  jornada55: {
    botPrompt: "Realizas alguna actividad importante durante el día?",
    input: selectField(['Sí', 'No']),
    answers: [
      { answer: 'Sí', nextId: 'nocturna' },
      { answer: 'No', nextId: 'matutina' }
    ]
  },

  jornada4: {
    botPrompt: "En qué horario trabajas?",
    input: selectField(['Por la mañana', 'Por la tarde', 'Por la noche']),
    answers: [
      { answer: 'Por la mañana', nextId: 'nocturna' },
      { answer: 'Por la tarde', nextId: 'matutina' },
      { answer: 'Por la noche', nextId: 'vespertina' }
    ]
  },

  nocturna: {
    botPrompt: "Bueno, tienes tiempo por la noche para ir a estudiar",
    answers: [{ nextId: 'jornada5' }]
  },
  matutina: {
    botPrompt: "Bueno, tienes tiempo por la mañana para ir a estudiar",
    answers: [{ nextId: 'jornada5' }]
  },
  vespertina: {
    botPrompt: "Bueno, tienes tiempo por la tarde y por la mañana para ir a estudiar, dependerá de si tienes compromisos en alguna de esas horas",
    answers: [{ nextId: 'jornada5' }]
  },

  jornada5: {
    botPrompt: 'Hemos llegado al final de las recomendaciones, ha sido un gusto <strong>@varName</strong>.',
    type: RTypes.TRANSFORMED_TEXT,
    varName: 'userName',
    input: endOfConversation()
  }
};


export default questions;
